/**
 * @file
 * Theme javascript.
 */

(function ($, once) {
  Drupal.behaviors.commerceCheckoutStepPanes = {
    attach(context) {
      once('commerceCheckoutStepPanes', 'form.commerce-checkout-flow-step-panes .layout-checkout-form', context).forEach(
        function (form) {
          $(form).on('drupalAjaxFormValidate', function (event) {
            const ajaxForm = event.target.form;
            if (ajaxForm && ajaxForm.reportValidity) {
              if (!ajaxForm.checkValidity()) {
                ajaxForm.reportValidity();
                return false;
              }
            }

            // Ensure to validate all other events before sumitting the ajax call.
            // Doesn't work with promised events like stripe paiements.
            const formEvents = $._data( event.target.form, "events" );
            var result = true;
            if (formEvents.submit) {
              const formSubmitEvents = formEvents.submit;
              if (formSubmitEvents.length) {
                formSubmitEvents.forEach(function(formEvent) {
                  result &= formEvent.handler(event);
                });
              }
            }
            return Boolean(result);
          });

          const events = ['click', 'mousedown', 'touch'];
          form.querySelectorAll('details:not([open=open])').forEach(function (details) {
            events.forEach( (event) => details.addEventListener('click', (e) => e.preventDefault()));
          })
        },
      );
    },
  };
})(jQuery, once);
