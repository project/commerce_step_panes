<?php

namespace Drupal\commerce_step_panes\Plugin\Derivative;

use Drupal\commerce_checkout\CheckoutPaneManager;
use Drupal\commerce_checkout\Entity\CheckoutFlow;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides pane plugin definitions for step panes.
 */
class StepPane extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Construct the checkout pane deriver.
   *
   * @param \Drupal\commerce_checkout\CheckoutPaneManager $paneManager
   *   The commerce checkout pane manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected readonly CheckoutPaneManager $paneManager,
    protected readonly EntityTypeManagerInterface $entityTypeManager
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.commerce_checkout_pane'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $checkout_flows = $this->entityTypeManager
      ->getStorage('commerce_checkout_flow')
      ->loadByProperties(['plugin' => 'step_panes']);

    if (empty($checkout_flows)) {
      return $this->derivatives;
    }

    foreach ($checkout_flows as $checkout_flow) {
      if ($checkout_flow instanceof CheckoutFlow) {
        $configuration = $checkout_flow->get('configuration');
        if (isset($configuration['step_panes_number'])) {
          for ($i = 0; $i < $configuration['step_panes_number']; $i++) {
            $step_pane_id = sprintf(
              'checkout_%s_step_pane_id_%d',
              $checkout_flow->id(),
              $i
            );
            $this->derivatives[$step_pane_id] = $base_plugin_definition;
          }
        }
      }
    }
    return $this->derivatives;
  }

}
