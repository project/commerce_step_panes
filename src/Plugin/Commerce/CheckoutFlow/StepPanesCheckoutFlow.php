<?php

namespace Drupal\commerce_step_panes\Plugin\Commerce\CheckoutFlow;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowWithPanesBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\Login;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides an accordion checkout flow.
 *
 * @CommerceCheckoutFlow(
 *   id = "step_panes",
 *   label = "Step panes",
 * )
 */
class StepPanesCheckoutFlow extends CheckoutFlowWithPanesBase {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'step_panes_number' => 1,
      'step_panes_first_label' => 'Order information',
    ];
  }

  /**
   * Fliter the panes that belongs to this worklfow.
   */
  public function getPanes() {
    $panes = parent::getPanes();
    foreach (array_keys($panes) as $pane_id) {
      $matches = [];
      if (preg_match('/checkout_([a-z_]+)_step_pane_id_([0-9]+)/', $pane_id, $matches)) {
        if (count($matches) > 1) {
          if ($matches[1] !== $this->parentEntity->id()) {
            unset($panes[$pane_id]);
          }
        }
      }
    }
    $this->panes = $panes;
    return $this->panes;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Ensure to update the number of validation step panes when reloaded.
    $this->paneManager->clearCachedDefinitions();
    return [
      'step_panes_number' => [
        '#type' => 'number',
        '#title' => 'Number of step panes',
        '#default_value' => $this->configuration['step_panes_number'] ?? 1,
      ],
      'step_panes_first_label' => [
        '#type' => 'textfield',
        '#title' => 'First step label',
        '#default_value' => $this->configuration['step_panes_first_label'] ?? '',
      ],
    ] + parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['step_panes_number'] = $values['step_panes_number'] ?? 1;
      $this->configuration['step_panes_first_label'] = $values['step_panes_first_label'] ?? '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSteps() {
    return [
      'login' => [
        'label' => $this->t('Login'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => FALSE,
      ],
      'order_information' => [
        'label' => $this->t('Order information'),
        'has_sidebar' => TRUE,
        'previous_label' => $this->t('Go back'),
      ],
      'review' => [
        'label' => $this->t('Review'),
        'next_label' => $this->t('Continue to review'),
        'previous_label' => $this->t('Go back'),
        'has_sidebar' => TRUE,
      ],
    ] + parent::getSteps();
  }

  /**
   * Get all panes for this step even if they are not visible at this pane step.
   *
   * @param string $step_id
   *   The pane step id.
   *
   * @return array|\Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface[]
   *   The panes grouped by step panes.
   */
  public function getGroupedStepPanes(string $step_id): mixed {
    $panes = $this->getPanes();
    $panes = array_filter($panes, fn($pane) =>
        /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface $pane */
        $pane->getStepId() == $step_id);
    $step_pane_id = NULL;
    $grouped_panes = [];
    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase $pane */
    foreach ($panes as $pane) {
      if (str_starts_with($pane->pluginId, 'step_pane')) {
        $step_pane_id = $pane->getId();
      }
      elseif ($step_pane_id !== NULL) {
        $grouped_panes[$step_pane_id][$pane->getId()] = $pane;
      }
    }
    return $grouped_panes;
  }

  /**
   * {@inheritdoc}
   */
  public function getStepId($requested_step_id = NULL) {
    $step = parent::getStepId($requested_step_id);
    if ($step !== 'complete' && !count($this->getVisiblePanes($requested_step_id))) {
      return $this->getPreviousStepId($requested_step_id);
    }

    return $step;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $step_id = NULL) {
    $form = CheckoutFlowBase::buildForm($form, $form_state, $step_id);

    $grouped_panes = $this->getGroupedStepPanes($step_id);
    if (!count($grouped_panes)) {
      return parent::buildForm($form, $form_state, $step_id);
    }

    if (!$form_state->has('pane_step')) {
      $pane_step = array_key_first($grouped_panes);
      $form_state->set('grouped_panes', $grouped_panes);
      $form_state->set('pane_step', $pane_step);
    }
    else {
      $pane_step = $form_state->get('pane_step');
    }

    $current_panes = $grouped_panes[$pane_step];
    $grouped_panes_ids = array_keys($grouped_panes);
    $last_grouped_pane_id = end($grouped_panes_ids);
    $current_step_pane = $this->getPane($pane_step);

    foreach ($grouped_panes as $group_pane_id => $group_panes) {
      $group_pane = $this->getPane($group_pane_id);
      $group_pane_configuration = $group_pane->getConfiguration();
      foreach ($group_panes as $pane_id => $pane) {
        unset($form[$pane_id]);
        if (!isset($form[$group_pane_id])) {
          $form[$group_pane_id] = [
            '#type' => 'details',
            '#open' => $pane_step === $group_pane_id,
            '#title' => $group_pane_configuration['label'] ?? $pane->getDisplayLabel(),
          ];
        }

        if (isset($current_panes[$pane_id]) && $pane->isVisible()) {
          $form[$group_pane_id][$pane_id] = [
            // Workaround for core bug #2897377.
            '#id' => Html::getId('edit-' . $pane_id),
            '#parents' => [$pane_id],
            '#theme' => 'commerce_checkout_pane',
            '#type' => $pane->getWrapperElement(),
            // '#title' => $pane->getDisplayLabel(),
            '#attributes' => [
              'class' => [
                'checkout-pane',
                'checkout-pane-' . str_replace('_', '-', (string) $pane_id),
              ],
            ],
            '#pane_id' => $pane_id,
          ];

          $form[$group_pane_id][$pane_id] = $pane->buildPaneForm($form[$group_pane_id][$pane_id], $form_state, $form[$group_pane_id]);
          // Avoid rendering an empty container.
          $form[$group_pane_id][$pane_id]['#access'] = (bool) Element::getVisibleChildren($form[$group_pane_id][$pane_id]);
        }

      }

      if ($group_pane_id === $pane_step) {
        if ($pane_step !== $last_grouped_pane_id) {
          $next_pane = $this->getNextStepPane($grouped_panes_ids, $group_pane_id);
          $form[$group_pane_id]['next'] = $this->buildNextPaneButton($current_panes, $next_pane);
        }
      }
    }

    $current_step_pane_configuration = $current_step_pane->getConfiguration();
    if (isset($current_step_pane_configuration['step_id'])) {
      $form['#step_id'] = $current_step_pane_configuration['step_id'];
    }

    if ($last_grouped_pane_id !== $pane_step) {
      $form['actions']['next']['#attributes']['disabled'] = 'disabled';
    }

    if ($this->hasSidebar($step_id)) {
      // The base class adds a hardcoded order summary view to the sidebar.
      // Remove it, there's a pane for that.
      unset($form['sidebar']);
      foreach ($this->getVisiblePanes('_sidebar') as $pane_id => $pane) {
        $form['sidebar'][$pane_id] = [
          // Workaround for core bug #2897377.
          '#id' => Html::getId('edit-' . $pane_id),
          '#parents' => ['sidebar', $pane_id],
          '#type' => $pane->getWrapperElement(),
          '#title' => $pane->getDisplayLabel(),
          '#attributes' => [
            'class' => ['checkout-pane', 'checkout-pane-' . str_replace('_', '-', $pane_id)],
          ],
        ];
        $form['sidebar'][$pane_id] = $pane->buildPaneForm($form['sidebar'][$pane_id], $form_state, $form);
      }
    }

    $form['#attached']['library'][] = 'commerce_step_panes/commerce_step_panes';
    return $form;
  }

  /**
   * Get the next pane button.
   *
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface[] $current_panes
   *   The current panes.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface $next_pane
   *   The next pane.
   *
   * @return mixed
   *   The built button.
   */
  protected function buildNextPaneButton(array $current_panes, CheckoutPaneInterface $next_pane) {
    $build = [
      '#type' => 'submit',
      '#value' => $this->t("Next: @pane_title", ['@pane_title' => $next_pane->getConfiguration()['label']]),
      '#validate' => ['::paneValidateCallback'],
      '#ajax' => [
        'callback' => '::paneStepCallback',
      ],
      '#attributes' => [
        'class' => [
          'button--primary',
          'checkout-next-step',
        ],
      ],
      '#prefix' => '<div class="checkout-pane__next-step">',
      '#suffix' => '</div>',
    ];

    foreach ($current_panes as $current_pane) {
      if ($current_pane instanceof Login) {
        $pane_configuration = $current_pane->getConfiguration();
        if (!$pane_configuration['allow_guest_checkout']) {
          $build['#op'] = 'login';
        }
      }
    }

    return $build;
  }

  /**
   * Get the next pane from the provided step.
   *
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface[] $panes
   *   All the panes of this order step.
   * @param string $pane_step
   *   The current pane step.
   *
   * @return false|\Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface
   *   The next step pane.
   */
  protected function getNextStepPane($panes, $pane_step) {
    $pos = array_search($pane_step, $panes);
    if ($pos < count($panes)) {
      return $this->getPane($panes[$pos + 1]);
    }
    return $this->getPane(end($panes));
  }

  /**
   * Manage form validate for each panes.
   *
   * @param array $form
   *   The form of the current step.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function paneValidateCallback(array $form, FormStateInterface $form_state): void {
    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface $pane_step */
    $pane_step = $form_state->get('pane_step');
    $grouped_panes = $form_state->get('grouped_panes');
    $panes = $grouped_panes[$pane_step];
    foreach ($panes as $pane_id => $pane) {
      if ($pane->isVisible()) {
        $pane->validatePaneForm($form[$pane_step][$pane_id], $form_state, $form[$pane_step]);
      }
    }

    if ($this->hasSidebar($form['#step_id'])) {
      foreach ($this->getVisiblePanes('_sidebar') as $pane_id => $pane) {
        if (!isset($form['sidebar'][$pane_id])) {
          continue;
        }
        if ($pane->isVisible()) {
          $pane->validatePaneForm($form['sidebar'][$pane_id], $form_state, $form);
        }
      }
    }
  }

  /**
   * Manage form submission for each panes.
   *
   * @param array $form
   *   The form of the current step.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $pane_step = $form_state->get('pane_step');

    $grouped_panes = $form_state->get('grouped_panes');
    $grouped_panes_ids = array_keys($grouped_panes);
    $last_grouped_pane_id = end($grouped_panes_ids);
    if ($grouped_panes === NULL || $last_grouped_pane_id === $pane_step) {
      return parent::submitForm($form, $form_state);
    }

    $this->refreshOrder();
    foreach ($grouped_panes as $grouped_pane) {
      foreach ($grouped_pane as $pane) {
        $pane->setOrder($this->order);
      }
    }

    $current_panes = $grouped_panes[$pane_step];
    if (!count($form_state->getErrors())) {
      foreach ($current_panes as $pane) {
        if ($pane->isVisible()) {
          $pane->submitPaneForm($form[$pane_step][$pane->getId()], $form_state, $form[$pane_step]);
        }
      }

      /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface $next_pane */
      $next_pane = $this->getNextStepPane($grouped_panes_ids, $pane_step);
      $pane_step = $form_state->set('pane_step', $next_pane->getId());

      $next_pane_configuration = $next_pane->getConfiguration();
      if (isset($next_pane_configuration['step_id'])) {
        $this->order->set('checkout_step', $next_pane_configuration['step_id']);
        $this->onStepChange($next_pane_configuration['step_id']);
      }

      $this->order->save();
    }

    $form_state->setRebuild(TRUE);
  }

  /**
   * Refresh the checkout page.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The portion of the render structure that will replace the form element.
   */
  public function paneStepCallback(array $form, FormStateInterface $form_state): AjaxResponse {
    return $this->ajaxRefreshForm($form, $form_state);
  }

  /**
   * Refresh the cached order between ajax calls.
   */
  protected function refreshOrder(): void {
    $this->_orderId = $this->order->id();
    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    $this->order = $order_storage->load($this->_orderId);
  }

}
