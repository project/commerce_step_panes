<?php

namespace Drupal\commerce_step_panes\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the completion message pane.
 *
 * @CommerceCheckoutPane(
 *   id = "step_pane",
 *   label = @Translation("Validation step pane"),
 *   default_step = "_disabled",
 *   deriver = "Drupal\commerce_step_panes\Plugin\Derivative\StepPane"
 * )
 */
class StepPane extends CheckoutPaneBase {

  /**
   * Get the step pane id.
   *
   * @return int
   *   Return the step pane id.
   */
  public function getStepPaneId() {
    return $this->pluginDefinition['step_pane_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'label' => '',
      'step_id' => 'order_information',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $pane_form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $steps = $this->checkoutFlow->getSteps();

    $step_ids = [];
    foreach ($steps as $step_id => $step) {
      $step_ids[$step_id] = $step['label'];
    }

    return [
      'label' => [
        '#type' => 'textfield',
        '#title' => "Pane label",
        '#default_value' => $configuration['label'] ?? '',
      ],
      'step_id' => [
        '#type' => 'select',
        '#title' => "Pane step id",
        '#description' => 'Which step id is needed when the step pane is loaded',
        '#required' => FALSE,
        '#options' => $step_ids,
        '#default_value' => $configuration['step_id'] ?? '',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['label'] = $values['label'] ?? '';
      $this->configuration['step_id'] = $values['step_id'] ?? '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    return $this->configuration['label'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    return FALSE;
  }

}
