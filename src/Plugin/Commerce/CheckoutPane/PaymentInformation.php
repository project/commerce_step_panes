<?php

namespace Drupal\commerce_step_panes\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentInformation as PaymentInformationBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * PaymentInformation class override to managed grouped panes.
 */
class PaymentInformation extends PaymentInformationBase {

  /**
   * {@inheritdoc}
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'];
    array_pop($parents);
    foreach ($form as $key => $form_element) {
      if (str_starts_with($key, 'step_pane:')) {
        $result_form = NestedArray::getValue($form_element, $parents);
        if ($result_form !== NULL) {
          return $result_form;
        }
      }
    }
    return NestedArray::getValue($form, $parents);
  }

}
